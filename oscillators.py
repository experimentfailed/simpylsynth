try:
    from waveforms import *
except:
    from .waveforms import *


class Oscillator(object):
    u""" Virtual oscillator object
     :param callable waveform: waveform of oscillator
     :param float volume: amplitude of generated wave (0.1 - 1.0)
     :param float tuning: transpose of frequency (for sub
     oscillator)
     """

    def __init__(self, waveform, volume, tuning=1.0):
        self.waveform = waveform
        self.volume = max(0.0, min(1.0, volume))
        self.tuning = tuning

    def generate_wave(self, phases):
        n = self.volume * self.waveform(phases * self.tuning)

        return n
