import numpy as np


class Player(object):
    def __init__(self, rate=44100):
        try:
            import pyaudio
            self.pyaudio = pyaudio.PyAudio()
            self.format = pyaudio.paInt16
        except ImportError:
            self.pyaudio = None

        self.stream = None

        # TODO: support stereo channels
        self.channels = 1
        self.rate = rate

    def open_stream(self, device_name=None, device_index=-1):
        u""" open audio output stream
        if neither device_name nor device_index is specified,
        default output device will be opened.
        :param str device_name: part of device name (ex: hw:0,0)
        :param int device_index: index of device
        """
        pyaudio = self.pyaudio

        if not pyaudio:
            raise ImportError(
                "Failed to import pyaudio, please install pyaudio"
            )

        if device_name:
            for n in range(pyaudio.get_device_count()):
                dev = pyaudio.get_device_info_by_index(n)
                if dev["name"].find(device_name) >= 0:
                    device = dev
                    break
            else:
                raise RuntimeError(
                    "audio device {} not found".format(device_name)
                )
        elif device_index >= 0:
            device = pyaudio.get_device_info_by_index(device_index)
        else:
            device = pyaudio.get_default_output_device_info()

        self.stream = pyaudio.open(
            channels=self.channels,
            format=self.format,
            rate=self.rate,
            output_device_index=device["index"],
            output=True,
        )

    def play_wave(self, wave):
        u""" play normalized wave
        :param numpy.array wave: normalized wave
        """
        if not self.stream:
            raise RuntimeError(
                "audio stream not opened; call open_stream() first."
            )

        wave = (wave * float(2 ** 15 - 1)).astype(np.int16).tobytes()
        self.stream.write(wave)
