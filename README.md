### SimpylSynth v0.1

#### License:
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


#### Description:
A simple, semi-modular (currently)monophonic, virtual audio synthesizer
for Python, based on the Synthesizer module by Yuma Mihira, links below.

Do be aware that I have no idea what I am doing. I do not have a strong
math background, so I'm relying on my advanced knowledge of Python, a
lot of help from the Internet (see sources, below), a bit of trial &
error, and some dumb luck!


#### Purpose:
Update: I've decided to write my music generator for REAPER, and thus
will be shelving this project. I may come back to it at a later date.

Original purpose text:
I am writing a procedural music generator (separate project). Decided
it would be best to produce audio directly with Python, rather than
exporting MIDI, as this way I can procedurally generate any/every aspect
of the sound I want. This plan lead me to the original Synthesizer
module, which got me most of the way there, and is simple enough to
easily hack together the extra features I want (or so I hope).



#### Known Issues:
 - I don't think I am mixing the oscillator volumes correctly yet, but
   what I have seems to more or less work, more or less sanely.


#### Installation:
Well IDK. How I use it, is clone the repo, and symlink the directory to 
other project directories where I need it.

For example:
```
git clone https://experimentfailed@bitbucket.org/experimentfailed/simpylsynth.git
cd /my/project/dir
ln -s /path/to/simpylsynth .
```


#### Usage Example:
```Python
from simpylsynth import *

player = Player()
player.open_stream()

synth = Synthesizer([
    Oscillator(sine, 0.5),
    Oscillator(square, 0.08, 0.51),
    Oscillator(triangle, 0.3, 1.001),
    Oscillator(sawtooth, 0.1, 1.002),
    Oscillator(white_noise, 0.03)
], masterVolume = 1.0)

plyer.play_wave( synth.generate_wave(440.0, 0.5) )
```

#### Plans & Goals:
 - Simple multi-track sequencer
 - Add at least low- and high-pass filters
 - Polyphony (or at least fake it with the sequencer)
 - Arpeggiator
 - LFOs, Envelopes
 - Some features may require a proper engine, so I may go that route


#### Sources:
Here are as many sources as I can keep track of, that have helped in my
work on this project:

 - https://github.com/yuma-m/synthesizer -- Original project source

 - https://pypi.org/project/synthesizer/ -- Original project's PyPI

 - https://stackoverflow.com/questions/32237769/defining-a-white-noise-process-in-python -- white noise

 - https://stackoverflow.com/questions/35764018/python-microphone-input-and-low-pass-filter-effect-realtime -- using a stream_callback...related solutions seem plentiful.
