from scipy import sin
from scipy.signal import sawtooth, square
from numpy.random import normal

sine = sin
sawtooth = sawtooth
square = square
triangle = lambda p, w=0.5: sawtooth(p, width=w)
white_noise = lambda p: normal(0, 1, size=len(p)) 
