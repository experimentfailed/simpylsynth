from scipy import signal, tile

def highpass(wave, sampleRate):
    filter_stop_freq = 700  # Hz
    filter_pass_freq = 1000  # Hz
    filter_order = 11

    nyquist_rate = sampleRate / 2.0

    # Apply high-pass filter
    return signal.filtfilt(
        signal.firls(
            filter_order,
            (0, filter_stop_freq, filter_pass_freq, nyquist_rate),
            (0, 0, 1, 1),
            nyq=nyquist_rate
        ),
        [1],
        wave
    )
