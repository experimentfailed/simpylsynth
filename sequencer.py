from time import sleep

freqs = ( # Chromatic scale
    440.0, 
    466.16,
    493.88,
    523.25,
    554.37,
    587.33,
    622.25,
    659.26, 
    698.46,
    739.99,
    783.99,
    830.61
)

notes = {
    "A":440.0,
    "A#":466.16,
    "Bb":466.16,
    "B":493.88,
    "C":523.25,
    "C#":554.37,
    "Db":554.37,
    "D":587.33,
    "D#":622.25,
    "Eb":622.25,
    "E":659.26,
    "F":698.46,
    "F#":739.99,
    "Gb":739.99,
    "G":783.99,
    "G#":830.61,
    "Ab":830.61
}
    

class Track:
    def __init__(self, notes=[], synth=None):
        self.synth = synth
        self.notes = notes


class Sequencer:
    def __init__(self, bpm=120, timeSig=(4,4), tracks=[Track()]):
        self.bpm = bpm
        self.timeSig = timeSig
        self.tracks = tracks

        self.state = 0

    def main(self):
        while True:
            state = self.state

            if state == 0:
                break
            elif state == -1:
                continue # pause
            elif state == 1:
                pass # play


if __name__ == "__main__":
    Sequencer().main()
    
    print( set(notes.values() ) )
