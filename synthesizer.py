# -*- coding: utf-8 -*-
"""To-Do:
 - Oscillator ADSR volume envelope
 - Sequencer
"""
import numpy as np

try:
    from oscillators import *
    from filters import *
    from player import Player
except:
    from .oscillators import *
    from .filters import *
    from .player import Player


class Synthesizer(object):
    def __init__(self,
        oscillators=[],
        filters=[],
        sampleRate=44100,
        masterVolume=1.0
    ):
        self.oscillators = oscillators
        self.filters = filters
        self.sampleRate = sampleRate
        self.masterVolume = masterVolume

    def generate_wave(self, frequency, duration):
        u"""Cenerate wave with constant frequency
        :param float frequency: frequency of wave
        :param float duration: duration of wave (seconds)
        :rtype: numpy.array
        :return: normalized wave
        """
        sampleRate = self.sampleRate
        oscillators = self.oscillators
        phaseDur = 1.01 / frequency # Need to take a closer look to see
        # why exactly the .01 seems to correct everything here...
        repeatSample = duration / phaseDur # How many times to play the
        # single-cycle sample to fill duration...not yet precise - needs
        # work
        
        phase = np.cumsum(
            2.0 * np.pi * frequency / sampleRate
            * np.ones( int( sampleRate * phaseDur ) )
        )

        try:
            sample = oscillators[0].generate_wave(phase)

            for osc in oscillators[1:]:
                sample += osc.generate_wave(phase)
                sample *= (1.0 / (1.0 + osc.volume))

            sample *= self.masterVolume
        except IndexError:
            raise(ValueError("No oscillators D:"))

        for f in self.filters:
            sample = f(sample, sampleRate)

        wave = np.tile(sample, round(repeatSample))

        # Actually, I think we need to tack on enough more of the sample
        # to make up any remaining duration, before returning (using
        # int() rather than round(), like above...but for now, this
        # proves the overall new approach is "fast enough", with at
        # least 4 oscillators and 1 filter on an Intel i7-6700K. And
        # here is where I will likely abandon this project. See
        # README.md for more info.

        return wave

    def generate_silence(self, duration):
        return np.zeros(int(self.sampleRate * duration))

    # From original module
    #~ def multi_voice(self, freqs, length):
        #~ u"""Generate wave consisting of multiple frequencies
        #~ :param list[float] freqs: list of frequencies
        #~ :param length: legnth of wave (seconds)
        #~ :rtype: numpy.array
        #~ :return: normalized wave
        #~ """
        #~ if not freqs or not isinstance(freqs, list):
            #~ raise ValueError("freqs must be a list of float")

        #~ wave = self.generate_wave(freqs[0], length)

        #~ for freq in freqs[1:]:
            #~ wave += self.generate_wave(freq, length)

        #~ wave /= float(len(freqs))

        #~ return wave


if __name__ == "__main__":
    from player import Player
    from matplotlib import pyplot as plt
    from tests import *
    from sequencer import freqs

    player = Player()
    player.open_stream()

    synth = Synthesizer(
        oscillators = [
            Oscillator(sine, 1.0),
            Oscillator(sawtooth, 1.0, 1.005),
            Oscillator(triangle, 1.0, 1.01),
            Oscillator(white_noise, 0.02),
        ],
        filters=[highpass],
        masterVolume = 1.00
    )

    testScale(player, synth, freqs)
    #~ wave = synth.generate_silence(1.0)

    #~ player.play_wave(wave)

    #~ print(len(wave))
    #~ for m,b in metronome(player, synth, 100):
        #~ pass
        #~ print(m,b)
        
