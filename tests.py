# Needs updating & synth module completed to support chords or whatever
from time import sleep

def test(player, synth, spd=0.3):
    chord = [439.93,440.005,440.2,439.95,440.001,440.01]
    player.play_wave(synth.multi_voice(chord, spd))

    sleep(spd/8)

    chord = [(i*2)/1.5 for i in chord]
    player.play_wave(synth.multi_voice(chord, spd/2))

    sleep(spd)

    chord = [261.626,  329.628, 391.996]
    player.play_wave( synth.multi_voice(chord, spd*8) )


def metronome(
    player,
    synth,
    bpm=120,
    timeSig=(4,4),
):
    t = (60 / bpm) / 2
    ts0, ts1 = timeSig
    beat = 1
    measure = 1

    while True:
        player.play_wave( synth.generate_wave(440.0, t) )
        player.play_wave( synth.generate_silence(t) )

        if beat < ts0:
            beat += 1
        else:
            measure += 1
            beat = 1

        yield measure, beat


def testScale(player, synth, freqs, bpm=480):
    t = (60 / bpm) / 2

    for f in freqs:
        player.play_wave( synth.generate_wave(f, t) )
